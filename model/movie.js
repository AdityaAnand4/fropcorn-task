/*
* Each movie that enters the collection should have the following data
* */

var mongoose = require('mongoose');

var movieSchema = new mongoose.Schema({
    movieName : String,
    added_time: { type: Date, default: Date.now }
});
var model=mongoose.model('content', movieSchema);
module.exports =model