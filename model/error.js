/*
 * Each error message that enters the collection should have the following data
 * */

var mongoose = require('mongoose');

var movieSchema = new mongoose.Schema({
    error: String,
    added_time: { type: Date, default: Date.now }
});
var model=mongoose.model('errorlogs', movieSchema);
module.exports =model