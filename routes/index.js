var express = require('express');
var router = express.Router();
var models=require('../model/movie')

/* GET home page. */
router.post('/', function(req, res, next) {
    if(req.body.movieName ){
        //Find if the movie name alread exists
            models.find({movieName :req.body.movieName },function(err,data){
                if(err){
                    console.log(err)
                }else{
                    if(data.length>0){
                        var error = new Error("Name already exists.");
                        next(error)
                    }else{
                        //Insert the movie name if the collection does not have that name
                        var model=new models();
                        model.movieName =req.body.movieName
                        model.save(function(err) {
                            if (err) {
                                console.log(err)
                            }
                            res.send({"Log":"Name added sucessfully."})
                        })
                }
            }
        })
    }else{
        var error = new Error("movieName is missing");
        next(error)
    }
});

module.exports = router;
